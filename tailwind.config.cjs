/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        dunes: "url('src/assets/images/great-sand-dunes.jpg')",
      }),
      fontFamily: {
        sans: ["monospace"],
      },
      colors: {
        white: "#ffffff",
        "rich-black": "#060a1e",
        "space-cadet": "#2B2D42",
        "jet": "#30323D",
        "tufts-blue": "#228CDB",
        "azure": "#357DED",
        "palatinate-blue": "#5438DC",
        "french-violet": "#731DD8",
      },
    },
    plugins: [],
  }
};
