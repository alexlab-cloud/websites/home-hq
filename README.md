# home-hq

[![Open in Dev Containers](https://img.shields.io/static/v1?label=Dev%20Containers&style=for-the-badge&message=Open&color=blue&logo=visualstudiocode)](https://vscode.dev/redirect?url=vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/alexlab-cloud/websites/home-hq)

A static website for hosting information at home, like a QR code for automatic guest WiFi login.

## Development

Install dependencies with `npm install` and start the development server with `npm run dev`.

## Deployment

Build the Docker image from the Dockerfile, then run `docker compose up -d` to serve the static website. It is accessible
on port `4901` by default.

## Customizing

Use an `.env` file like the `.env.example` to supply environment variables. Add and remove buttons, headers, design features,
and anything else as you please! This is a very basic website built to serve a simple purpose.

---

<sub>Emoji used for repository logo designed by OpenMoji – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

---
