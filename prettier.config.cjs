module.exports = {
  tabWidth: 2,
  useTabs: false,
  tailwindConfig: "./tailwind.config.cjs",
  trailingComma: 'es5',
  semi: false,
  singleQuote: true,
  quoteProps: 'consistent',
  bracketSpacing: true,
  arrowParens: 'always',
  printWidth: 100,
  plugins: [
    'prettier-plugin-astro',
  ],
  overrides: [
    {
      files: '*.astro',
      options: {
        parser: 'astro',
      },
    },
  ],
};
